map -docstring "leave insert mode" \
    global insert <a-j> <esc>

map -docstring "delete buffer" \
    global user d :db<ret>

map -docstring "next buffer" \
    global user n :buffer-next<ret>

map -docstring "previous buffer" \
    global user p :buffer-previous<ret>

map -docstring "quit" \
    global user q :q<ret>

map -docstring "force quit" \
    global user <a-q> :q!<ret>

map -docstring "save" \
    global user s :w<ret>

map -docstring "save & quit" \
    global user S :wq<ret>

map -docstring "save-all" \
    global user <a-s> :wa<ret>

map -docstring "save-all & quit" \
    global user <a-S> :waq<ret>

{
  # add local bin and conditionally yarn bin
  typeset -U path

  type -p yarn \
  && path=($(yarn global bin) $path)

  if [[ -d /home/linuxbrew ]]; then
    path=(/home/linuxbrew/.linuxbrew/bin $path)
  elif [[ -d $HOME/.linuxbrew ]]; then
    path=($HOME/.linuxbrew/bin $path)
  fi

  path=($HOME/.local/bin $path)

  XDG_CONFIG_HOME="$HOME/.config"
  export XDG_CONFIG_HOME

  type -p kak &&
    {
      VISUAL=kak
      export VISUAL
      EDITOR=kak
      export EDITOR
    }

  type -p qutebrowser &&
    {
      BROWSER=qutebrowser
      export BROWSER
    }

  type -p wine &&
    {
      WINEDEBUG=-all
      export WINEDEBUG
    }

  type -p fzf && {
    FZF_ALT_C_COMMAND="find . -mindepth 1 -type d"
    export FZF_ALT_C_COMMAND

    type -p rg && {
      rg_cmd="rg -uuu --files -g ''"
      FZF_DEFAULT_COMMAND=${rg_cmd}
      FZF_CTRL_T_COMMAND=${rg_cmd}
      export FZF_DEFAULT_COMMAND FZF_CTRL_T_COMMAND
      unset rg_cmd
    }
  }
} &> /dev/null

if [[ ${TTY} == "/dev/tty1" && -f $HOME/.x ]]; then
  exec $HOME/.x
fi

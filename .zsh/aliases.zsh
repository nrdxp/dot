#!/usr/bin/zsh

# setup tilde expansion
for dir in Downloads Games; do
  [[ -d $HOME/$dir ]] && hash -d $dir=$HOME/$dir
done

# virsh alias
type -p virsh && {
  alias vrsh="virsh --connect qemu:///system"
  alias vrshl="vrsh list"
  alias vrsha="vrsh list --all"
}

# cp with info
type -p rsync &&
  alias cp="rsync --info=progress2"

# shortcut to the editor command
[[ -v EDITOR ]] && alias v="$EDITOR"

# manage dotfiles
[[ -d $HOME/.dot ]] \
&& alias dot="/usr/bin/env git --git-dir=$HOME/.dot --work-tree=$HOME"

# hub alias to git
type -p hub &&
  eval "$(hub alias -s)"

# restart zsh
alias rz="exec zsh"

type -p systemctl && {
  alias sctl="systemctl"
  alias ssctl="sudo systemctl"
  alias uctl="systemctl --user"
  alias jctl="journalctl"
}

# cd aliases
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."

# shorten grep
alias gi="grep -Ei"

type -p tmux && {
  alias tm="tmux new-session -s $(whoami)"
  alias ta="tmux attach -t $(whoami)"
}

alias dir="dir --color=auto"

# use exa as ls when available
type -p exa &&
  alias ls="exa"

alias le="ls -lh --color=always | less"
alias lea="ls -lah --color=always | less"
alias grep="grep --color=auto"
alias dmesg="dmesg --color=always"

alias l="ls -l --git --group --header"
alias lx="l -Gx"
alias la="ls -l --all --git --group --header"
alias lax="la -Gx"

# different tree command depending on whether exa exists
type -p exa &&
  alias t="l -T --color=always | less" ||
  alias t="tree -hC | less"

alias se="sudoedit"
alias df="df -h"
alias du="du -h -c"
alias sudo='sudo -E '

# pgrep alias
type -p pgrep &&
  alias pg="pgrep"

# pkill alias
type -p pkill &&
  alias pk="pkill"

# Arch linux specific
type -p pacman && {
  alias p="pacman"

  # reinstall all native packages
  alias reinstall!="pacman -Qnq | sudo pacman -S -"

  type -p yaourt && {
    alias y="yaourt"
    alias orph="yaourt -Qtdq | sudo pacman -Rns -"
  }

  type -p reflector &&
    alias upmir="sudo reflector -p https --country 'United States' --age 12 \
    --sort rate --save /etc/pacman.d/mirrorlist"
}

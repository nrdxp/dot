# set colorscheme
[[ -f $HOME/.base16_theme ]] && source $HOME/.base16_theme

#namespace to hide stdout
{


# Zsh options
setopt extendedglob
setopt incappendhistory
setopt sharehistory
setopt histignoredups
setopt histreduceblanks
setopt histignorespace
setopt histallowclobber
setopt autocd
setopt cdablevars
setopt nomultios
setopt pushdignoredups
setopt autocontinue
setopt promptsubst

# Environment
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=$HOME/.history
DIRSTACKSIZE=5
export LESS=-XR

# set the locale
LANG=en_US.UTF-8
LC_ALL=en_US.UTF-8
export LC_ALL LANG

# Sources
. $HOME/.zsh/aliases.zsh

# Reccommend package on command not found; Arch Linux
type -p pkgfile &&
. /usr/share/doc/pkgfile/command-not-found.zsh

# user defined functions
if [[ -d $HOME/.zfunctions ]]; then
typeset -U fpath
fpath=($HOME/.zfunctions $fpath)

# add linuxbrew completions to fpath if they exist
if [[ -d /home/linuxbrew/.linuxbrew/Homebrew/completions/zsh ]]; then
  fpath=(/home/linuxbrew/.linuxbrew/Homebrew/completions/zsh $fpath)
elif [[ -d $HOME/.linuxbrew/Homebrew/completions/zsh ]]; then
  fpath=($HOME/.linuxbrew/Homebrew/completions/zsh $fpath)
fi

for file in $HOME/.zfunctions/*; do
  autoload -Uz $file
done
fi

# source local config not synced to git repo
if [[ -f $HOME/.zshlocal ]]; then
. $HOME/.zshlocal
fi

# source antibody plugins if they exist
() {
local plugins="$HOME/.zsh/plugins.zsh"

[[ -f $plugins ]] &&
  () {
    . $plugins

    # history substring search keybinds
    bindkey '^[OA' history-substring-search-up
    bindkey '^[OB' history-substring-search-down

    zle -N zle-line-init
  }
}


# load fzf config
[ -f ~/.fzf.zsh ] && {
source ~/.fzf.zsh

# if in tmux launch fzf in tmux pane
[[ ${+TMUX} == 1 ]] \
  && FZF_TMUX=1
}

#load compinit
autoload -Uz compinit
typeset -i updated_at=$(date +'%j' -r ~/.zcompdump ||
stat -f '%Sm' -t '%j' ~/.zcompdump)
if [ $(date +'%j') != $updated_at ]; then
compinit
else
compinit -C
fi

# Case insens only when no case match; after all completions loaded
zstyle ':completion:*' matcher-list \
'' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'

# Auto rehash for new binaries
zstyle ':completion:*' rehash true

# Key binds
bindkey "jj" vi-cmd-mode
bindkey -v '^?' backward-delete-char

# keep shell state frozen
ttyctl -f


} > /dev/null

# set colorscheme if it exists and was not set at the top
type -w base16_snazzy &> /dev/null \
&& {
  [[ -f ~/.base16_theme ]] || base16_snazzy
}
